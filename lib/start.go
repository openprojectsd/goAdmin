package lib
import (
	"goAdmin/lib/admin"
	"goAdmin/lib/cilent"
	"net/http"
)

// AdminStartPage Admin Login Processing private
func AdminStartPage(w http.ResponseWriter, r *http.Request) {
 	admin.StartPage(w,r)
}

// CilentStartPage Not Login Processing public
func CilentStartPage(w http.ResponseWriter, r *http.Request) {
	cilent.StartPage(w,r)
}