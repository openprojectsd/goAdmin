package System


type ServerConfig struct {
	Server struct {
		UseHTTPS bool `json:"UseHttps"`
		UsePort string `json:"UsePort"`
		Key      struct {
			Publickey  string `json:"publickey"`
			Privatekey string `json:"privatekey"`
		} `json:"Key"`
		APIName    string `json:"ApiName"`
		RouterPath struct {
			StaticRouteNot string `json:"StaticRouteNot"`
			StaticPath string `json:"StaticPath"`
			StaticRoute string `json:"StaticRoute"`
			AdminRoute  string `json:"AdminRoute"`
			CilentRoute string `json:"CilentRoute"`
		} `json:"RouterPath"`
	} `json:"Server"`
}

