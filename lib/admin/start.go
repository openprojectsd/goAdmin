package admin
import (
	"fmt"
	"net/http"
)
func StartPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the AdminPage! "+r.RequestURI)
	fmt.Println("Endpoint Hit: AdminPage")
}
