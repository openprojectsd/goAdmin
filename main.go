package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"goAdmin/lib"
	"goAdmin/lib/entities/System"
	"net/http"
	"os"
	"strings"
)


func errorPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Blank Page! "+r.RequestURI)
}
func neuter(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, "/") {
			http.NotFound(w, r)
			return
		}
		next.ServeHTTP(w, r)
	})
}
func handleRequests(config System.ServerConfig) {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.PathPrefix(config.Server.RouterPath.CilentRoute).HandlerFunc(lib.CilentStartPage)
	myRouter.PathPrefix(config.Server.RouterPath.AdminRoute).HandlerFunc(lib.AdminStartPage);
	fileServer := http.FileServer(http.Dir(config.Server.RouterPath.StaticPath))
	myRouter.Handle(config.Server.RouterPath.StaticRouteNot, http.NotFoundHandler())
	myRouter.PathPrefix(config.Server.RouterPath.StaticRoute).Handler(http.StripPrefix(config.Server.RouterPath.StaticRoute, neuter(fileServer)))

	myRouter.PathPrefix("/").HandlerFunc(errorPage);
	if config.Server.UseHTTPS==true {
		err := http.ListenAndServeTLS(config.Server.UsePort, config.Server.Key.Publickey, config.Server.Key.Privatekey, myRouter)
		if err != nil {
			fmt.Println(err);
		}
	}else {
		http.ListenAndServe(config.Server.UsePort,myRouter);
	}
}
func check(e error) bool {
	if e != nil {
		panic(e);
		fmt.Println("Error Configration")
		return false;
	}
	return true;
}
func main() {
	dat, err := os.ReadFile("sslkey\\Program.json")
	if check(err) == true {

		var config System.ServerConfig
		err := json.Unmarshal(dat, &config)
		if err != nil {
			fmt.Println(err)
		}
		handleRequests(config)
		return
	}
	fmt.Print(err)
}
